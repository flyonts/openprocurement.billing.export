from datetime import datetime


class SQLWriter(object):
    def __init__(self, csvfile, fieldnames, table_name, restval='', extrasaction='raise',
                 dialect='mysql', engine='MyISAM', charset='utf8', drop_table=True, *args, **kwds):
        self.csvfile = csvfile
        self.fieldnames = fieldnames
        self.table_name = table_name
        self.restval = restval
        self.extrasaction = extrasaction
        self.dialect = dialect
        self.engine = engine
        self.charset = charset
        self.drop_table = drop_table
        self.type_mapping = {
            '.amount': 'DECIMAL(20,2)',
        }
        self.auto_increment = 0

    def write(self, fmt, *args):
        if args:
            self.csvfile.write(fmt % args)
        else:
            self.csvfile.write(fmt)
        self.csvfile.write("\r\n")

    def writeheader(self):
        self.write("-- SQLWriter export %s", datetime.now().isoformat())
        self.write("-- ")
        self.write("-- Table structure for table `%s`", self.table_name)
        self.write("-- ")
        if self.drop_table:
            self.write("DROP TABLE IF EXISTS `%s`;", self.table_name)

        self.write("CREATE TABLE `%s` (", self.table_name)
        self.write("    `id` int(11) NOT NULL AUTO_INCREMENT,")

        for field in self.fieldnames:
            field_type = 'VARCHAR(250)'
            for k, t in self.type_mapping.items():
                if k in field:
                    field_type = t
            if '.' in field:
                field = field.replace('.', '_')
            self.write("    `%s` %s DEFAULT NULL,", field, field_type)

        self.write("    PRIMARY KEY (`id`)")
        self.write(") DEFAULT CHARSET=%s;", self.charset)
        self.write("-- ")
        self.write("-- Dumping data for table `%s`", self.table_name)
        self.write("-- ")

    def writerow(self, row):
        self.auto_increment += 1
        sql = "INSERT INTO `%s` VALUES (%d" % (self.table_name, self.auto_increment)
        for field in self.fieldnames:
            if field not in row and self.extrasaction == 'raise':
                raise ValueError('field not found {}'.format(field))
            value = row.get(field, None)
            if value is None:
                sql += ", NULL"
            elif isinstance(value, (int, float)):
                sql += ", {}".format(value)
            else:
                sql += ", '{}'".format(value)
        sql += ");"
        self.write(sql)

    def writerows(self, rows):
        for r in rows:
            self.writerow(r)
