# -*- coding: utf-8 -*-
import os
import sys
import csv
import yaml
import hashlib
import logging
import argparse
import requests
from time import sleep
from ConfigParser import ConfigParser
from couchdb import Server, Session
from .models import Tender, Initial, ExportObj, parse_local_date
from .writer import SQLWriter


__version__ = '0.1.beta'
module = sys.modules['__main__'].__file__
logger = logging.getLogger(module)
SESSION = requests.Session()


def parse_command_line(argv):
    formatter_class = argparse.RawDescriptionHelpFormatter
    parser = argparse.ArgumentParser(description=module,
                                     formatter_class=formatter_class)
    parser.add_argument('--version', action='version',
                        version='%(prog)s {}'.format(__version__))
    parser.add_argument('-v', '--verbose', dest='verbose_count',
                        action='count', default=0,
                        help='for more verbose use multiple times')
    parser.add_argument('-l', '--log',
                        type=argparse.FileType('at'), default=sys.stderr,
                        help='redirect log to a file')
    parser.add_argument('-o', '--output',
                        type=argparse.FileType('w'), default=sys.stdout,
                        help='redirect output to a file')
    parser.add_argument('-c', '--config', default='openprocurement.api.ini',
                        help='default openprocurement.api.ini')
    parser.add_argument('-k', '--section', default='app:api',
                        help='section name in config, default [app:api]')
    parser.add_argument('-F', '--format', default='CSV',
                        help='default CSV, supported SQL')
    parser.add_argument('-T', '--table', default='exportbids',
                        help='table name for SQL format, default `exportbids`')
    parser.add_argument('-f', '--field', action='append',
                        help='specify fields to export (use multiple times)')
    parser.add_argument('-a', '--after', metavar='TENDER_ID',
                        help='start tenderID in format UA-YYYY-MM-DD')
    parser.add_argument('-b', '--before', metavar='TENDER_ID',
                        help='end tenderID in format UA-YYYY-MM-DD')
    parser.add_argument('-s', '--start', metavar='START_DATE',
                        help='start Date in format YYYY-MM-DD')
    parser.add_argument('-e', '--end', metavar='END_DATE',
                        help='end Date in format YYYY-MM-DD')
    parser.add_argument('-S', '--status', action='append',
                        help='filter by tender status (default any)')
    parser.add_argument('-B', '--bid-status', action='append',
                        help='filter by bid status (default any)')
    parser.add_argument('-I', '--ignore', action='append',
                        help='ignore some tenders by tender.id (not tenderID)')
    args = parser.parse_args(argv[1:])
    # decode date args
    if args.format not in ('CSV', 'SQL'):
        logger.error("Bad export format, use CSV or SQL")
        sys.exit(1)
    try:
        if args.start:
            args.start = parse_local_date(args.start)
        if args.end:
            args.end = parse_local_date(args.end)
    except:
        logger.error("Bad start or end date in command line")
        sys.exit(1)
    return args


def get_with_retry(url, doc_hash='', require_text=''):
    for i in range(5):
        try:
            logger.debug("GET {}".format(url))
            resp = SESSION.get(url, timeout=30)
            resp.raise_for_status()
            if require_text and require_text not in resp.text:
                raise ValueError('bad response require_text not found')
            if doc_hash and ':' in doc_hash:
                hash_type, hash_value = doc_hash.split(':', 1)
                hexdigest = hashlib.new(hash_type, resp.text).hexdigest()
                assert hash_value == hexdigest, 'doc hash not match'
            return resp.text
        except KeyboardInterrupt:
            raise
        except Exception as e:
            logger.error("{}: {}".format(e.__class__.__name__, e))
            if i > 3:
                raise
        for s in range(i * 100):
            sleep(0.1)


def force_unicode(s, onerror=u""):
    if not s:
        return u""
    try:
        u = s.decode('utf-8')
    except UnicodeEncodeError:
        u = onerror
    return u


def load_initial_bids(tender, export_lots=True):
    initial_bids = {}
    audit_title = "audit_{}".format(tender.id)

    for doc in tender.documents:
        logger.debug("Process document {}".format(force_unicode(doc.title, doc.id)))
        # if doc.author and doc.author != 'auction':
        #     logger.debug("Skip doc by author {}".format(doc.author))
        #     continue
        if not doc.title.startswith(audit_title):
            continue

        audit_yaml = get_with_retry(doc.url, doc_hash=getattr(doc, 'hash', ''))
        if 'initial_bids' not in audit_yaml:
            continue

        audit = yaml.load(audit_yaml)
        if audit['tender_id'] != tender.id:
            raise ValueError("bad tender.id in {}".format(doc.title))
        if audit['tenderId'] != tender.tenderID:
            raise ValueError("bad tenderID in {}".format(doc.title))

        bids_loaded = 0
        for bid in audit['timeline']['auction_start']['initial_bids']:
            audit_id = "{}_{}".format(audit['id'], bid['bidder'])
            initial_bids[audit_id] = bid
            bids_loaded += 1

        logger.debug("Load {} initial bids for {}".format(bids_loaded, audit['id']))

    if not initial_bids:
        logger.warning("audit.yaml not found for tender {} status {}".format(tender.id, tender.status))

    return initial_bids


def process_tender_bids(tender, output, fieldnames, bid_status, export_lots=True):
    initial_bids = load_initial_bids(tender, export_lots)

    bids_processed = 0
    lots_processed = 0

    for bid in tender.bids:
        if bid_status and bid.status not in bid_status:
            logger.debug("Ignore {}/bids/{} by bid status {}".format(
                         tender.id, bid.id, bid.status))
            continue

        audit_id = "{}_{}".format(tender.id, bid.id)
        if audit_id in initial_bids:
            bid.initial = Initial().import_data(initial_bids[audit_id])
        elif initial_bids and not getattr(bid, 'lotValues', None):
            logger.warning("Initial bid for {} not found".format(audit_id))

        if getattr(bid, 'value', None) or not getattr(bid, 'lotValues', None) or not export_lots:
            obj = ExportObj(tender=tender, bid=bid)
            row = obj.export_fields(fieldnames)
            output.writerow(row)
            bids_processed += 1

        if export_lots and getattr(bid, 'lotValues', None):
            for lot in bid.lotValues:
                audit_id = "{}_{}_{}".format(tender.id, lot.relatedLot, bid.id)
                if audit_id in initial_bids:
                    lot.initial = Initial().import_data(initial_bids[audit_id])
                elif initial_bids:
                    logger.warning("Initial bid for {} not found".format(audit_id))

                obj = ExportObj(tender=tender, bid=bid, lot=lot)
                row = obj.export_fields(fieldnames)
                output.writerow(row)
                lots_processed += 1

    logger.debug("Processed {} bids {} lots".format(bids_processed, lots_processed))

    return bids_processed, lots_processed


def export_tenders(args):
    config = ConfigParser()
    config.read(args.config)
    settings = dict(config.items(args.section))

    db_name = os.environ.get('DB_NAME', settings['couchdb.db_name'])
    server = Server(settings.get('couchdb.url'), session=Session(retry_delays=range(10)))
    db = server[db_name]

    if not args.field:
        args.field = ['bid.id', 'bid.status', 'bid.value.amount', 'bid.initial.amount', 'bid.owner',
                      'lot.relatedLot', 'lot.value.amount', 'lot.initial.amount']

    export_lots = False
    for field in args.field:
        if field.startswith('lot.'):
            export_lots = True
            break

    if args.format == 'CSV':
        output = csv.DictWriter(args.output, fieldnames=args.field)
    elif args.format == 'SQL':
        output = SQLWriter(args.output, fieldnames=args.field, table_name=args.table)
    else:
        raise ValueError('Not supported export format, use CSV or SQL')
    output.writeheader()

    total_tenders = 0
    total_bids = 0
    total_lots = 0

    for docid in db:
        doc = db.get(docid)
        if doc.get('doc_type') != 'Tender':
            continue
        tender = Tender().import_data(doc, partial=True)
        if not tender.tenderID:
            raise ValueError("Bad tenderID {}".format(docid))
        if args.after and tender.tenderID < args.after:
            logger.debug("Ignore {} by tenderID {}".format(docid, tender.tenderID))
            continue
        if args.before and tender.tenderID > args.before:
            logger.debug("Ignore {} by tenderID {}".format(docid, tender.tenderID))
            continue
        if args.start and tender.date < args.start:
            logger.debug("Ignore {} by date {}".format(docid, tender.date.isoformat()))
            continue
        if args.end and tender.date > args.end:
            logger.debug("Ignore {} by date {}".format(docid, tender.date.isoformat()))
            continue
        if args.status and tender.status not in args.status:
            logger.debug("Ignore {} by status {}".format(docid, tender.status))
            continue
        if not tender.bids:
            logger.debug("Ignore {} by empty bids".format(docid))
            continue
        if args.ignore and tender.id in args.ignore:
            logger.debug("Ignore {} by tender.id in command line args".format(docid))
            continue
        logger.info("Tender {} {}".format(docid, tender.tenderID))
        bids, lots = process_tender_bids(tender, output, args.field, args.bid_status,
                                         export_lots=export_lots)
        total_bids += bids
        total_lots += lots
        total_tenders += 1

    logger.info("Total {} tenders with {} bids and {} lots".format(total_tenders,
                total_bids, total_lots))


def main():
    args = parse_command_line(sys.argv)
    level = max(3 - args.verbose_count, 0) * 10
    logging.basicConfig(stream=args.log, level=level,
                        format='%(asctime)-15s %(levelname)s %(message)s')
    logger.setLevel(level)

    try:
        export_tenders(args)
    except KeyboardInterrupt:
        logger.error('Program interrupted!')
    finally:
        logging.shutdown()


if __name__ == '__main__':
    sys.exit(main())
