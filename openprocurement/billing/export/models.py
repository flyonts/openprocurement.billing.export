import os
from pytz import timezone
from datetime import datetime
from iso8601 import parse_date, ParseError
from couchdb_schematics.document import SchematicsDocument
from schematics.exceptions import ConversionError
from schematics.models import Model
from schematics.types import StringType, FloatType, BooleanType, BaseType, MD5Type, URLType
from schematics.types.compound import ListType, ModelType as BaseModelType


TZ = timezone(os.environ['TZ'] if 'TZ' in os.environ else 'Europe/Kiev')


def parse_local_date(s):
    date = parse_date(s, None)
    if not date.tzinfo:
        date = TZ.localize(date)
    return date


class IsoDateTimeType(BaseType):
    MESSAGES = {
        'parse': u'Could not parse {0}. Should be ISO8601.',
    }

    def to_native(self, value, context=None):
        if isinstance(value, datetime):
            return value
        try:
            date = parse_date(value, None)
            if not date.tzinfo:
                date = TZ.localize(date)
            return date
        except ParseError:
            raise ConversionError(self.messages['parse'].format(value))
        except OverflowError as e:
            raise ConversionError(e.message)

    def to_primitive(self, value, context=None):
        return value.isoformat()


class ExportMixin(object):
    def export_fields(self, fieldnames):
        out = {}
        for field in fieldnames:
            root = self
            for part in field.split('.'):
                root = getattr(root, part, None)
                if not root:
                    break
            if isinstance(root, BaseType):
                out[field] = root.to_primitive()
            else:
                out[field] = root
        return out


class ExportObj(ExportMixin):
    def __init__(self, **kw):
        for k, v in kw.items():
            setattr(self, k, v)


class Initial(Model):
    amount = FloatType(required=True, min_value=0)  # Amount as a number.
    date = IsoDateTimeType()
    id = MD5Type(required=True)


class Value(Model):
    amount = FloatType()  # Amount as a number.
    currency = StringType()  # The currency in 3-letter ISO 4217 format.
    valueAddedTaxIncluded = BooleanType()


class ModelType(BaseModelType):
    # disable default strict mode for partial data
    def __init__(self, model_class, **kwargs):
        BaseModelType.__init__(self, model_class, **kwargs)
        if getattr(self, 'strict', False):
            self.strict = False


class LotValue(Model):
    value = ModelType(Value, required=True)
    relatedLot = MD5Type()
    participationUrl = URLType()
    date = IsoDateTimeType()


class Document(Model):
    id = MD5Type(required=True)
    hash = StringType()
    documentType = StringType()
    title = StringType(required=True)  # A title of the document.
    title_en = StringType()
    title_ru = StringType()
    description = StringType()  # A description of the document.
    description_en = StringType()
    description_ru = StringType()
    format = StringType(required=True, regex='^[-\w]+/[-\.\w\+]+$')
    url = StringType(required=True)  # Link to the document or attachment.
    datePublished = IsoDateTimeType()
    dateModified = IsoDateTimeType()  # Date that the document was last dateModified
    language = StringType()
    documentOf = StringType()
    relatedItem = MD5Type()
    # author = StringType()


class Parameter(Model):
    code = StringType(required=True)
    value = FloatType(required=True)


class Bid(Model):
    # tenderers = ListType(ModelType(Organization))
    parameters = ListType(ModelType(Parameter))
    lotValues = ListType(ModelType(LotValue), default=list())
    date = IsoDateTimeType()
    id = MD5Type(required=True)
    status = StringType()
    value = ModelType(Value)
    documents = ListType(ModelType(Document), default=list())
    participationUrl = URLType()
    owner_token = StringType()
    transfer_token = StringType()
    owner = StringType()


class Tender(SchematicsDocument, Model):
    bids = ListType(ModelType(Bid), default=list())
    date = IsoDateTimeType()
    tenderID = StringType()
    documents = ListType(ModelType(Document), default=list())  # All documents and attachments related to the tender.
    procurementMethodType = StringType()
    owner = StringType()
    status = StringType()
