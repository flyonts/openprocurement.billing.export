import re
from setuptools import setup, find_packages

version = re.search(
    r'__version__\s*=\s*[\'"]([^\'"]+)[\'"]',
    open('openprocurement/billing/export/__init__.py').read()
).group(1)

requires = [
    'couchdb-schematics',
    'iso8601',
    'pytz',
    'PyYAML',
    'requests',
    'setuptools',
]
test_requires = requires + [
    'python-coveralls',
]
entry_points = {
    'console_scripts': [
        'exportbids=openprocurement.billing.export:main',
    ]
}

setup(
    name='openprocurement.billing.export',
    version=version,
    description="Command line tool for export billing data from bids",
    long_description=open("README.md").read(),
    # Get more strings from
    # http://pypi.python.org/pypi?:action=list_classifiers
    classifiers=[
        "License :: OSI Approved :: Apache Software License",
        "Programming Language :: Python",
    ],
    platforms=['posix'],
    keywords='openprocurement',
    author='Volodymyr Flonts',
    author_email='flyonts@gmail.com',
    url='https://github.com/openprocurement/openprocurement.billing.export',
    license='Apache License 2.0',
    packages=find_packages(),
    namespace_packages=['openprocurement', 'openprocurement.billing'],
    include_package_data=True,
    zip_safe=False,
    install_requires=requires,
    tests_require=test_requires,
    # test_suite="openprocurement.billing.export.tests.main.suite",
    entry_points=entry_points
)
