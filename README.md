# OpenProcurement Billing Export

Command line tool for export billing data from bids

## Installation

	$ python bootstrap.py
	$ bin/buildout -N

## Copyright

(c) 2017 Volodymyr Flonts <flyonts@gmail.com>
